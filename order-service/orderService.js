const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');

const app = express();
const port = 3002;

app.use(bodyParser.json());

let orders = [];

// GET /orders
app.get('/orders', (req, res) => {
  res.json(orders);
});

// POST /orders
app.post('/orders', async (req, res) => {
  const order = req.body;

  // Memeriksa apakah pengguna valid dengan memanggil User Service
  try {
    const response = await axios.get('http://localhost:3001/users');
    const users = response.data;

    const userExists = users.some(user => user.id === order.userId);

    if (userExists) {
      orders.push(order);
      res.status(201).json(order);
    } else {
      res.status(400).json({ error: 'Invalid user ID' });
    }
  } catch (error) {
    res.status(500).json({ error: 'Failed to communicate with User Service' });
  }
});

app.listen(port, () => {
  console.log(`Order Service running on port ${port}`);
});
